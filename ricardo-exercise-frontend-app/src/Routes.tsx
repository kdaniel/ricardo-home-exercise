import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { HomePage } from 'views/HomePage/HomePage';
import { Search } from 'views/Search/Search';
import { SingleArticle } from 'views/Article/SingleArticle';

const Routes = () => {

  return (
    <Switch>
      <Route exact path='/'>
        <HomePage />
      </Route>
      <Route exact path='/search/:searchText'>
        <Search />
      </Route>
      <Route exact path='/article/:articleId'>
        <SingleArticle />
      </Route>
      <Redirect to="/" />
    </Switch>
  );
};

export { Routes };
