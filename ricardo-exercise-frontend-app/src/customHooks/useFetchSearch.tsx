import { useQuery } from 'react-query';
import { SearchResponse } from 'types/searchTypes';
import { getSearchResults } from 'services/services';

/**
 * Custom hook to fetch all articles and total count for a given search text
 *
 * @param searchTerm The search keyword.
 */
export default function useFetchSearch(searchTerm: string) {
  return useQuery<SearchResponse>([ 'search', searchTerm ], () => getSearchResults(searchTerm));
}
