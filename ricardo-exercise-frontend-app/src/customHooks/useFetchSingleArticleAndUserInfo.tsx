import { useQuery } from 'react-query';
import { ArticleResponse } from 'types/articleResponseType';
import { GetUserResponse } from 'types/userType';
import { getArticleDetails, getUserInfo } from 'services/services';


/**
 * Custom hook to fetch all details for a single article and relevant user information
 *
 * @param articleId The id of the article
 */
export default function useFetchSingleArticleAndUserInfo(articleId: string) {
  const articleQueryResult =
    useQuery<ArticleResponse>([ 'article', articleId ], () => getArticleDetails(articleId));

  const userId = articleQueryResult?.data?.sellerId;

  const userQueryResult = useQuery<GetUserResponse>([ 'user', userId ], () => getUserInfo(userId),
    {
      enabled: !!userId,
    },
  );

  return { articleQueryResult, userQueryResult };
}
