import * as React from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { createWrapper } from 'tests/utils';
import useFetchSingleArticleAndUserInfo from 'customHooks/useFetchSingleArticleAndUserInfo';
import { mocked } from 'ts-jest/utils';
import { getArticleDetails, getUserInfo } from 'services/services';
import { ArticleDetails } from 'types/articleResponseType';
import { User } from 'types/userType';

jest.mock('../../services/services');

describe('useFetchSingleArticleAndUserInfo query hook', () => {
  test('successful query hook', async () => {
    jest.mock('../../services/services');
    const mockArticleDetails: ArticleDetails = {
      id: 'test-article-id',
      title: 'test-article-title',
      subtitle: 'test-article-subtitle',
      price: 99,
      descriptionHtml: 'test-article-description',
      imageUrl: 'test-article-image',
      sellerId: 'test-article-sellerId',
    };

    const mockUserInfo: User = {
      id: 'test-article-sellerId',
      name: 'test-user-name',
    };
    mocked(getArticleDetails).mockResolvedValueOnce(mockArticleDetails);
    mocked(getUserInfo).mockResolvedValueOnce(mockUserInfo);

    const { result, waitFor } = renderHook(() => useFetchSingleArticleAndUserInfo('test'), {
      wrapper: createWrapper(),
    });

    await waitFor(() => result.current.articleQueryResult.isSuccess);
    await waitFor(() => result.current.userQueryResult.isSuccess);

    expect(result.current.articleQueryResult.data?.title).toBe(mockArticleDetails.title);
    expect(result.current.userQueryResult.data?.name).toBe(mockUserInfo.name);
  });

  test('unsuccessful query hook', async () => {

    mocked(getArticleDetails).mockRejectedValueOnce(new Error('error'));
    const { result, waitFor } = renderHook(() => useFetchSingleArticleAndUserInfo('test'), {
      wrapper: createWrapper(),
    });

    await waitFor(() => result.current.articleQueryResult.isError);
    expect(result.current.articleQueryResult).toBeDefined();
  });


});
