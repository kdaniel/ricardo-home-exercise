import * as React from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { createWrapper } from 'tests/utils';
import useFetchSearch from '../useFetchSearch';
import { mocked } from 'ts-jest/utils';
import { getSearchResults } from 'services/services';
import { SearchResponse } from 'types/searchTypes';

jest.mock('../../services/services');

describe('useFetchSearch query hook', () => {
  test('successful query hook', async () => {
    jest.mock('../../services/services');
    const mockResult: SearchResponse = {
      totalCount: 1,
      articles: [
        {
          id: 1,
          buyNowPrice: 99,
          imageUrl: 'test-img',
          title: 'test',
          endDate: '2021-07-27T08:43:00Z',
        },
      ],
    };
    mocked(getSearchResults).mockResolvedValueOnce(mockResult);
    const { result, waitFor } = renderHook(() => useFetchSearch('test'), {
      wrapper: createWrapper(),
    });

    await waitFor(() => result.current.isSuccess);

    expect(result.current.data?.totalCount).toBe(1);
    expect(result.current.data?.articles).toBe(mockResult.articles);
  });

  test('unsuccessful query hook', async () => {

    mocked(getSearchResults).mockRejectedValueOnce(new Error('error'));
    const { result, waitFor } = renderHook(() => useFetchSearch('test'), {
      wrapper: createWrapper(),
    });

    await waitFor(() => result.current.isError);
    expect(result.current.error).toBeDefined();
  });


});
