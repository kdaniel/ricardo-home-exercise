import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import { CssBaseline, MuiThemeProvider } from '@material-ui/core';
import { ReactQueryDevtools } from 'react-query/devtools';
import theme from 'theme';
import { Routes } from './Routes';
import { TopBar } from 'components/TopBar';

const browserHistory = createBrowserHistory();
const queryClient = new QueryClient();

function App() {
  return (
    <MuiThemeProvider theme={ theme }>
      <CssBaseline />
      <QueryClientProvider client={ queryClient }>
        <Router history={ browserHistory }>
          <TopBar />
          <Routes />
        </Router>
        <ReactQueryDevtools initialIsOpen position={ 'top-right' } />
      </QueryClientProvider>
    </MuiThemeProvider>
  );
}

export default App;
