import axios from 'axios';
import { ArticleResponse } from 'types/articleResponseType';
import { GetUserResponse } from 'types/userType';
import { SearchResponse } from 'types/searchTypes';

const API_KEY = process.env.REACT_APP_API_TOKEN;
const BASE_URL = process.env.REACT_APP_API_URL;

export const getSearchResults = async (searchText: string) => {
  const { data } = await axios.get(`${BASE_URL}/search`, {
    params: {
      searchText,
      apiToken: API_KEY
    },
  });

  return data as SearchResponse;
};

export const getArticleDetails = async (articleId: string) => {
  const { data } = await axios.get(`${ BASE_URL }/article-details`, {
    params: {
      articleId,
      apiToken: API_KEY,
    },
  });

  return data as ArticleResponse;
};

export const getUserInfo = async (userId: string | undefined) => {
  const { data } = await axios.get(`${ BASE_URL }/user`, {
    params: {
      userId,
      apiToken: API_KEY,
    },
  });

  return data as GetUserResponse;
};