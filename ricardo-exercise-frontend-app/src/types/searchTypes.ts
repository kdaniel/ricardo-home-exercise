export type SearchArticle = {
  id: number;
  title: string;
  endDate: string;
  imageUrl: string;
  buyNowPrice: number;
}

export type SearchResponse = {
  articles: SearchArticle[];
  totalCount: number;
}
