export type ArticleDetails = {
  id: string;
  title: string;
  subtitle: string;
  price: number;
  descriptionHtml: string;
  imageUrl: string;
  sellerId: string;
}

export type ArticleResponse = ArticleDetails;
