import React from 'react';
import { Grid } from '@material-ui/core';
import { SearchForm } from 'views/HomePage/SearchForm';

/**
 * The landing page component
 */
export const HomePage = () => {

  return (
    <Grid style={ { marginTop: '10%' } }>
      <SearchForm />
    </Grid>
  );
};
