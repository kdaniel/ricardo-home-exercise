import React from 'react';
import { Button, Grid, makeStyles, Typography } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { Field, Form, Formik } from 'formik';
import { useHistory } from 'react-router';
import { FormTextField } from 'components/FormTextField';
import { searchValidationSchema } from 'views/HomePage/validationSchema/searchValidationSchema';
import theme from 'theme';

const useStyles = makeStyles(
  {
    form: {
      display: 'flex',
    },
    container: {
      marginRight: theme.spacing(6),
      marginLeft: theme.spacing(6),
    },
    buttonContainer: {
      display: 'flex',
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(1),
    },
    button: {
      justifyContent: 'space-between',
    },
    icon: {
      width: theme.spacing(2.2),
      marginRight: theme.spacing(1),
    },
  },
);

/**
 * Component to render the text field and the button as a form
 */
export const SearchForm = () => {
  const history = useHistory();
  const classes = useStyles();

  const searchArticles = (searchTerm: string) => {
    history.push(`/search/${ searchTerm }`);
  };

  return (
    <Formik
      initialValues={ { searchTerm: '' } }
      validationSchema={ searchValidationSchema }
      onSubmit={ values => searchArticles(values.searchTerm) }
    >
      { ({ handleSubmit, values }) =>
        <Form onSubmit={ handleSubmit } className={ classes.form }>
          <Grid container spacing={ 2 } className={ classes.container }>
            <Grid item xs={ 11 }>
              <Field
                name='searchTerm'
                component={ FormTextField }
              />
            </Grid>
            <Grid item className={ classes.buttonContainer }>
              <Button
                id={ 'search-button' }
                data-testid={ 'search-button' }
                type={ 'submit' }
                variant="outlined"
                disabled={ !values.searchTerm }
                color={ 'primary' }
                className={ classes.button }
              >
                <SearchIcon className={ classes.icon } />
                <Typography>SEARCH</Typography>
              </Button>
            </Grid>
          </Grid>
        </Form>
      }
    </Formik>
  );
};
