import React from 'react';
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react';
import { SearchForm } from 'views/HomePage/SearchForm';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

describe('SearchForm component', () => {

  it('Displays form', () => {
    render(<SearchForm />);
    const textField = screen.getByTestId('search-field');
    const searchButton = screen.getByTestId('search-button');

    // Verify the form is rendered
    expect(textField).toBeInTheDocument();
    expect(searchButton).toBeInTheDocument();
  });

  it('Form actions', async () => {
    const history = createMemoryHistory();
    const historySpy = jest.spyOn(history, 'push');
    render(
      <Router history={ history }>
        <SearchForm />
      </Router>,
    );

    const textField = screen.getByTestId('search-field');
    const searchButton = screen.getByTestId('search-button');

    // Verify search button is disabled when text field is empty
    expect(searchButton).toBeDisabled();

    act(() => {
      fireEvent.change(textField, { target: { value: 'bicycle' } });
    });

    await waitFor(() => {
      // Verify search button is  not disabled when text field is not empty
      expect(searchButton).not.toBeDisabled();
    });

    act(() => {
      fireEvent.click(searchButton);
    });

    await waitFor(() => {
      expect(historySpy).toHaveBeenCalledWith('/search/bicycle');
    });
  });

});
