import React from 'react';
import { HomePage } from 'views/HomePage/HomePage';
import { render, screen } from '@testing-library/react';

describe('HomePage component', () => {

  it('Displays form', () => {
    render(<HomePage />);
    const textField = screen.getByTestId('search-field');
    const searchButton = screen.getByTestId('search-button');

    // Verify the form is rendered
    expect(textField).toBeInTheDocument();
    expect(searchButton).toBeInTheDocument();
  });

});
