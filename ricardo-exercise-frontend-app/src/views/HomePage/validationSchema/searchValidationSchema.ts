import * as yup from 'yup';

export const searchValidationSchema = yup.object().shape({
  searchTerm: yup.string().nullable().required('Search text is required')
})
