import { waitFor } from '@testing-library/react';
import { renderWithClient } from 'tests/utils';
import React from 'react';
import { SingleArticle } from 'views/Article/SingleArticle';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import { ArticleDetails } from 'types/articleResponseType';
import { User } from 'types/userType';
import { mocked } from 'ts-jest/utils';
import { getArticleDetails, getUserInfo } from 'services/services';

jest.mock('../../../services/services');

describe('query component', () => {
  test('successful query component', async () => {

    const mockArticleDetails: ArticleDetails = {
      id: 'test-article-id',
      title: 'test-article-title',
      subtitle: 'test-article-subtitle',
      price: 99,
      descriptionHtml: 'test-article-description',
      imageUrl: 'test-article-image',
      sellerId: 'test-article-sellerId',
    };

    const mockUserInfo: User = {
      id: 'test-article-sellerId',
      name: 'test-user-name',
    };

    mocked(getArticleDetails).mockResolvedValueOnce(mockArticleDetails);
    mocked(getUserInfo).mockResolvedValueOnce(mockUserInfo);
    const history = createMemoryHistory();

    history.push(`/article/${ mockArticleDetails.id }`);

    const result = renderWithClient(<Router history={ history }><SingleArticle /></Router>);

    await waitFor(() => result.getByText('test-user-name'));

    // Verify image is on the screen
    expect(result.getByTestId('article-single-card-media')).toHaveStyle(`background-image: url(${ mockArticleDetails.imageUrl })`);

    // Verify components shows all fields
    // title
    expect(result.getByText('test-article-title')).toBeInTheDocument();
    // seller
    expect(result.getByText('test-user-name')).toBeInTheDocument();
    // price
    expect(result.getByText('99 CHF')).toBeInTheDocument();
    // subtitle
    expect(result.getByText('test-article-subtitle')).toBeInTheDocument();
    // description
    expect(result.getByText('test-article-description')).toBeInTheDocument();

  });

  test('unsuccessful query component', async () => {

    mocked(getArticleDetails).mockRejectedValueOnce({});
    mocked(getUserInfo).mockRejectedValueOnce({});
    const history = createMemoryHistory();

    history.push('/article/testId');

    const result = renderWithClient(<Router history={ history }><SingleArticle /></Router>);

    await waitFor(() => result.getByText('Error loading content'));

    // Verify modal opens
    expect(result.getByText('Error loading content')).toBeInTheDocument();

  });

});
