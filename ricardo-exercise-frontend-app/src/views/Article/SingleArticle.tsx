import React from 'react';
import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  CircularProgress,
  Container,
  createStyles,
  Divider,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { useParams } from 'react-router';
import useFetchSingleArticleAndUserInfo from 'customHooks/useFetchSingleArticleAndUserInfo';
import ReactHtmlParser from 'react-html-parser';
import { ErrorDialog } from 'components/ErrorDialog';
import theme from 'theme';

const useStyles = makeStyles((themes) =>
  createStyles({
    media: {
      height: 0,
      width: '100%',
      paddingTop: '100%',
      backgroundSize: 'contain',

    },
    circularProgress: {
      top: '25%',
      left: '50%',
      position: 'absolute',
    },
    container: {
      padding: 0,
      marginTop: themes.spacing(10),
    },
    divider: {
      marginLeft: theme.spacing(4),
      marginRight: theme.spacing(4),
    },
  }),
);

/**
 * Component to render a single article
 */
export const SingleArticle = () => {
  const { articleId } = useParams();
  const { articleQueryResult, userQueryResult } = useFetchSingleArticleAndUserInfo(articleId);
  const { data: article, isLoading: isArticleLoading, error: articleError } = articleQueryResult;
  const { data: user, isLoading: isUserLoading, error: userError } = userQueryResult;

  const classes = useStyles();
  return (
    <>
      { ( articleError || userError ) && <ErrorDialog redirectTo={ '/' } /> }
      { ( isArticleLoading || isUserLoading ) &&
      <CircularProgress size={ 70 } className={ classes.circularProgress } /> }
      { article && (
        <Container maxWidth={ 'lg' } className={ classes.container }>
          <Grid container spacing={ 5 } justify={ 'center' }>
            <Grid container spacing={ 5 }>
              <Grid item xs={ 6 }>
                <Card
                  square
                  id={`single-article-card-${articleId}`}
                  data-testid={`single-article-card-${articleId}`}
                >
                  <CardMedia
                    id={ 'article-single-card-media' }
                    data-testid={ 'article-single-card-media' }
                    className={ classes.media }
                    image={ article.imageUrl }
                    title={ article.title }
                  />
                </Card>
              </Grid>
              <Grid item xs={ 6 }>
                <Card square>
                  <CardHeader
                    title={ article.title }
                    titleTypographyProps={ { variant: 'h3' } }
                  />
                  <CardContent>
                    <Divider className={ classes.divider } style={ { marginTop: 0, marginBottom: theme.spacing(2) } } />
                    <Grid container direction={ 'column' }>
                      <Grid item >
                        { user?.name &&
                        <>
                            <Typography variant={ 'body2' } display='inline'>{ `Seller: ` }</Typography>
                            <Typography display='inline'>{ user?.name }</Typography>
                        </>
                        }
                      </Grid>
                      <Grid item>
                        { article.price &&
                        <>
                            <Typography variant={ 'body2' } display='inline'>{ `Price: ` }</Typography>
                            <Typography display='inline'>{ `${ article.price } CHF` }</Typography>
                        </>
                        }
                      </Grid>
                    </Grid>
                    <Divider className={ classes.divider } style={ { marginTop: theme.spacing(2) } } />
                    <Grid container direction={ 'column' } style={ { marginTop: theme.spacing(2) } }>
                      { article.subtitle &&
                      <Grid item>
                          <Typography> { article.subtitle }</Typography>
                      </Grid>
                      }
                      <Grid item>
                        {/*use span component to keep styling but to avoid warning */ }
                        <Typography component={ 'span' }> { ReactHtmlParser(article.descriptionHtml) }</Typography>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      )
      }
    </>
  );
};
