import React from 'react';
import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  createStyles,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import moment from 'moment';
import { SearchArticle } from 'types/searchTypes';
import theme from 'theme';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      maxWidth: 350,
      height: '100%',
    },
    media: {
      height: 0,
      width: '100%',
      paddingTop: '100%',
      backgroundSize: 'contain',
      backgroundColor: '#e4e1dc',
    },
    title: {
      minHeight: theme.spacing(8),
    },
  }),
);

type Props = {
  article: SearchArticle;
}

/**
 * Component to show one small article on the results page
 *
 *  @param article The article to show
 */
export const ArticleSmall = ({ article }: Props) => {
  const classes = useStyles();
  const { id, title = '-', imageUrl, buyNowPrice = '-', endDate } = article;
  const formattedEndDate = endDate ? moment(endDate).format('YYYY-MM-DD [at] HH:mm:ss') : '-';

  return (
    <Grid key={ id } item xs={ 3 }>
      <Card
        id={ `article-small-card-${ id }` }
        data-testid={ `article-small-card-${ id }` }
        className={ classes.root }
        square
      >
        <CardActionArea
          id={ 'article-small-action-area' }
          data-testid={ 'article-small-action-area' }
          href={ `/article/${ id }` }
        >
          <CardMedia
            id={ 'article-small-card-media' }
            data-testid={ 'article-small-card-media' }
            className={ classes.media }
            image={ imageUrl }
            title={ title }
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2" className={ classes.title }>
              { title }
            </Typography>
            <Typography display="inline">
              { 'Ending on: ' }
            </Typography>
            <Typography variant="body2" display="inline">
              { formattedEndDate }
            </Typography>
          </CardContent>
          <CardActions>
            <Typography variant={ 'h5' }>{ buyNowPrice ? `${ buyNowPrice } CHF` : '' }</Typography>
          </CardActions>
        </CardActionArea>
      </Card>
    </Grid>
  );
};

