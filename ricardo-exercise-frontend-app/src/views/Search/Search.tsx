import React from 'react';
import { CircularProgress, Grid, makeStyles } from '@material-ui/core';
import { useParams } from 'react-router';
import useFetchSearch from 'customHooks/useFetchSearch';
import { SearchResults } from 'views/Search/SearchResult';
import { ErrorDialog } from 'components/ErrorDialog';

const useStyles = makeStyles({
  circularProgress: {
    top: '25%',
    left: '50%',
    position: 'absolute',
  },
});

/**
 * Container component to render all search results, error, or the loading spinner.
 */
export const Search = () => {
  const { searchText } = useParams();
  const classes = useStyles();

  const { data, isLoading, error } = useFetchSearch(searchText);

  return (
    <Grid>
      { error && <ErrorDialog redirectTo={ '/' } /> }
      { isLoading && <CircularProgress size={ 70 } className={ classes.circularProgress } /> }
      { data && <SearchResults searchText={ searchText } articles={ data.articles } totalCount={ data.totalCount } /> }
    </Grid>
  );
};
