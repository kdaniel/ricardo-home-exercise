import React from 'react';
import { Container, createStyles, Grid, makeStyles, Typography } from '@material-ui/core';
import { ArticleSmall } from 'views/Search/ArticleSmall';
import { SearchArticle } from 'types/searchTypes';
import theme from 'theme';

const useStyles = makeStyles((themes) =>
  createStyles({
    container: {
      padding: 0,
      marginTop: themes.spacing(10),
    },
  }),
);

type Props = {
  articles: SearchArticle[];
  totalCount: number;
  searchText: string;
}

/**
 * Component to render all search results.
 *
 *  @param articles The articles to show
 *  @param totalCount The number of articles
 *  @param searchText The search term
 */
export const SearchResults = ({ articles, totalCount, searchText }: Props) => {
  const classes = useStyles();

  return (
    <Container maxWidth={ 'lg' } className={ classes.container }>
      <Grid container spacing={ 3 } justify={ 'center' }>
        <Grid item xs={ 12 } style={ { paddingLeft: 0, marginBottom: theme.spacing(3) } }>
          <Typography variant={ 'body2' }>{ `Showing results for: '${ searchText }'` }</Typography>
          <Typography variant={ 'body2' }>{ `Total count: ${ totalCount }` }</Typography>
        </Grid>
        <Grid container spacing={ 2 }>
          { articles.map((article) => (
            <ArticleSmall key={ article.id } article={ article } />
          )) }
        </Grid>
      </Grid>
    </Container>
  );
};
