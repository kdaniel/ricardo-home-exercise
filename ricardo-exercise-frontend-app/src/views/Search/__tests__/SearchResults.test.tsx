import React from 'react';
import { render, screen } from '@testing-library/react';
import moment from 'moment';
import { SearchResponse } from 'types/searchTypes';
import { SearchResults } from 'views/Search/SearchResult';

describe('SearchResults component', () => {

  const testSearchResponse: SearchResponse = {
    articles: [
      {
        id: 1,
        title: 'test-title-1',
        endDate: moment().toISOString(),
        imageUrl: 'test-image',
        buyNowPrice: 99,
      },
      {
        id: 2,
        title: 'test-title-2',
        endDate: moment().subtract(1, 'days').toISOString(),
        imageUrl: 'test-image',
        buyNowPrice: 199,
      },
      {
        id: 3,
        title: 'test-title-3',
        endDate: moment().subtract(2, 'days').toISOString(),
        imageUrl: 'test-image',
        buyNowPrice: 299,
      },
    ],
    totalCount: 3,
  };

  it('Displays cards, total count, and search word', () => {
    render(
      <SearchResults
        searchText={ 'test-search-text' }
        totalCount={ testSearchResponse.totalCount }
        articles={ testSearchResponse.articles }
      />,
    );

    // Verify total count and search word is on the page
    expect(screen.getByText(`Showing results for: \'test-search-text\'`)).toBeInTheDocument();
    expect(screen.getByText(`Total count: ${ testSearchResponse.totalCount }`)).toBeInTheDocument();

    // Verify cards are rendered
    expect(screen.getByTestId(`article-small-card-${ testSearchResponse.articles[0].id }`)).toBeInTheDocument();
    expect(screen.getByTestId(`article-small-card-${ testSearchResponse.articles[1].id }`)).toBeInTheDocument();
    expect(screen.getByTestId(`article-small-card-${ testSearchResponse.articles[2].id }`)).toBeInTheDocument();

  });

});
