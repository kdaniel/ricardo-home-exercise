import { waitFor } from '@testing-library/react';
import { renderWithClient } from 'tests/utils';
import React from 'react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import { mocked } from 'ts-jest/utils';
import { getSearchResults } from 'services/services';
import { Search } from 'views/Search/Search';
import { SearchResponse } from 'types/searchTypes';

jest.mock('../../../services/services');

describe('query component', () => {
  test('successful query component', async () => {

    const mockSearchResponse: SearchResponse = {
      articles: [
        {
          id: 1,
          endDate: '2021-07-27T08:43:00Z',
          title: 'test-article-title',
          buyNowPrice: 99,
          imageUrl: 'test-article-image',
        },
      ],
      totalCount: 1,
    };

    mocked(getSearchResults).mockResolvedValueOnce(mockSearchResponse);
    const history = createMemoryHistory();
    history.push('/search/test');

    const result = renderWithClient(<Router history={ history }><Search /></Router>);

    await waitFor(() => result.getByText('Total count: 1'));

    // Verify all data is on the screen
    expect(result.getByText('Total count: 1')).toBeInTheDocument();
    expect(result.getByTestId('article-small-card-1')).toBeInTheDocument();
    expect(result.getByText('test-article-title')).toBeInTheDocument();
    expect(result.getByText('2021-07-27 at 10:43:00')).toBeInTheDocument();

  });

  test('unsuccessful query component', async () => {

    mocked(getSearchResults).mockRejectedValueOnce({});
    const history = createMemoryHistory();
    history.push('/search/test');

    const result = renderWithClient(<Router history={ history }><Search /></Router>);

    await waitFor(() => result.getByText('Error loading content'));

    // Verify modal opens
    expect(result.getByText('Error loading content')).toBeInTheDocument();

  });

});
