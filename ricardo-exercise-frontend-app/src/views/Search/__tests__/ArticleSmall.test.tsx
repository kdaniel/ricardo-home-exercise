import React from 'react';
import { render, screen } from '@testing-library/react';
import moment from 'moment';
import { ArticleSmall } from 'views/Search/ArticleSmall';
import { SearchArticle } from 'types/searchTypes';

describe('ArticleSmall component', () => {

  const testArticle: SearchArticle = {
    id: 1,
    title: 'test-title',
    endDate: moment().toISOString(),
    imageUrl: 'test-image',
    buyNowPrice: 99,
  };

  it('Displays card', () => {
    render(<ArticleSmall article={ testArticle } />);

    // Verify all data is on the screen
    expect(screen.getByText(testArticle.title)).toBeInTheDocument();
    expect(screen.getByTestId('article-small-card-media')).toHaveStyle(`background-image: url(${ testArticle.imageUrl })`);
    expect(screen.getByText(moment(testArticle.endDate).format('YYYY-MM-DD [at] HH:mm:ss'))).toBeInTheDocument();
    expect(screen.getByText(`${ testArticle.buyNowPrice } CHF`)).toBeInTheDocument();
    expect(screen.getByTestId('article-small-action-area').closest('a')).toHaveAttribute('href', `/article/${testArticle.id}`)
  });

});
