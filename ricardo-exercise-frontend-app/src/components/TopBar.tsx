import React from 'react';
import { AppBar, Grid, makeStyles, Toolbar } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from 'assets/icons/ricardo-logo.svg';
import theme from 'theme';

const useStyles = makeStyles(
  {
    logoContainer: {
      marginTop: theme.spacing(4),
      marginLeft: theme.spacing(4),
    },
    logo: {
      height: '60px',
    },
  },
);

/**
 * The top navigation bar component.
 */
export const TopBar = () => {
  const classes = useStyles();

  return (
    <Grid container>
      <AppBar color={ 'transparent' } elevation={ 0 } position="static">
        <Toolbar>
          <Grid item className={ classes.logoContainer }>
            <Link to="/">
              <Logo className={ classes.logo } id={'ricardo-logo'} data-testid={'ricardo-logo'}/>
            </Link>
          </Grid>
        </Toolbar>
      </AppBar>
    </Grid>
  );
};
