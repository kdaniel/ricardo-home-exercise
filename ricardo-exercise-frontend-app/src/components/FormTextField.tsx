import React from 'react';
import { createStyles, makeStyles, TextField, TextFieldProps } from '@material-ui/core';
import { FieldProps } from 'formik';

const useStyles = makeStyles((theme) =>
  createStyles({
    textInput: {
      padding: 0,
      '& .MuiOutlinedInput-input': {
        padding: theme.spacing(2),
      },
    },
  }),
);

/**
 * The text field component used in forms.
 *
 *  @param props Props for the TextField
 */

export const FormTextField: React.FC<FieldProps & TextFieldProps> = (props) => {
  // use to show form helper text on error (touched or empty)
  // const isTouched = getIn(props.form.touched, props.field.name);
  // const errorMessage = getIn(props.form.errors, props.field.name);
  const classes = useStyles();
  const { error, helperText, field, form, ...rest } = props;

  return (
    <TextField
      id={'search-field'}
      variant="outlined"
      label="Search text"
      placeholder="Search for articles"
      fullWidth
      className={ classes.textInput }
      size={ 'medium' }
      margin="normal"
      // use to show form helper text on error (touched or empty)
      // error={ error ?? Boolean(isTouched && errorMessage) }
      // helperText={ helperText ?? ( ( isTouched && errorMessage ) ? errorMessage : undefined ) }
      InputLabelProps={ {
        shrink: true,
      } }
      inputProps={{ "data-testid": "search-field" }}
      InputProps={ {
        className: classes.textInput,
      } }
      { ...rest }
      { ...field }
    />
  );
};
