import React from 'react';
import { Button, Dialog, DialogActions, DialogContent, Divider, Link, makeStyles, Typography } from '@material-ui/core';


const useStyles = makeStyles((theme) => ( {
  dialogPaper: {
    maxWidth: '400px',
  },
  content: {
    '& > *:not(:first-child)': {
      marginTop: theme.spacing(2),
    },
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
  },
  title: {
    marginTop: theme.spacing(1.5),
    textTransform: 'uppercase',
    lineHeight: 1,
  },
  divider: {
    flex: 1,
  },
} ));


type Props = {
  redirectTo: string;
}

/**
 * Error dialog component to show on API error.
 *
 *  @param redirectTo The route to redirect to on click on
 */
export const ErrorDialog = ({ redirectTo }: Props) => {
  const classes = useStyles();

  return (
    <Dialog
      id='error-dialog'
      maxWidth={ false }
      open
      fullWidth
      aria-labelledby='error-dialog'
      PaperProps={ { square: true } }
      classes={ { paper: classes.dialogPaper } }
    >
      <DialogContent className={ classes.content }>
        <Typography variant='h5' color='error' className={ classes.title }>
          Error
        </Typography>
        <Divider className={ classes.divider } />
        <Typography>
          Error loading content
        </Typography>
      </DialogContent>
      <DialogActions>
        <Link href={ redirectTo }>
          <Button variant='contained' color='primary' data-testid={ 'error-dialog-ok-button' } id={ 'error-dialog-ok-button' }>
            Ok
          </Button>
        </Link>
      </DialogActions>
    </Dialog>
  );
};
