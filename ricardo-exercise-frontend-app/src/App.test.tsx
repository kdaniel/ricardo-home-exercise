import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const logo = screen.getByTestId('ricardo-logo');
  const textField = screen.getByTestId('search-field');
  const searchButton = screen.getByTestId('search-button');

  expect(logo).toBeInTheDocument();
  expect(textField).toBeInTheDocument();
  expect(searchButton).toBeInTheDocument();
});
