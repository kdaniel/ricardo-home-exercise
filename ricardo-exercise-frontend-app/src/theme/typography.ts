const typography = {
  body2: {
    color: 'black',
    fontSize: '1rem',
    lineHeight: 1.5,
    letterSpacing: '0.00938em',
    fontWeight: '600',
  },
}

export default typography;
