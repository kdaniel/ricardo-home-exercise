import { colors } from '@material-ui/core';

const palette = {
  background: {
    default: colors.grey['200'],
  },
};

export default palette;
