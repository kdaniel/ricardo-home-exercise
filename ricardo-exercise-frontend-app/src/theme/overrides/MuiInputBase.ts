import { colors } from '@material-ui/core';

const MuiInputBase = {
  input: {
    background: 'none',
    '&::placeholder': {
      color: colors.grey[600],
    },
    color: '#4f4f4f',
  },
};

export default MuiInputBase;
