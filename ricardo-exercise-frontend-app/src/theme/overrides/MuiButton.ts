const MuiButton = {
  outlinedPrimary: {
    color: '#8999db',
    borderColor: '#8999db',
    borderWidth: 2,
    backgroundColor: '#e8e8ed',
  },
};

export default MuiButton;
