import MuiInputBase from 'theme/overrides/MuiInputBase';
import MuiOutlinedInput from 'theme/overrides/MuiOutlinedInput';
import MuiFormLabel from 'theme/overrides/MuiFormLabel';
import MuiButton from 'theme/overrides/MuiButton';

const overrides = {
  MuiInputBase,
  MuiOutlinedInput,
  MuiFormLabel,
  MuiButton,
}
export default overrides;
