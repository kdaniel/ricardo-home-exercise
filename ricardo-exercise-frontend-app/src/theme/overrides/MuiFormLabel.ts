import { colors } from '@material-ui/core';

const MuiFormLabel = {
  root: {
    color: colors.grey[600],
    '&$focused': {
      color: '#8999db',
    },
  },
};

export default MuiFormLabel;
