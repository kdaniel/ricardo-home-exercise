import { colors } from '@material-ui/core';

const MuiOutlinedInput = {
  root: {
    '&$focused $notchedOutline': {
      // borderColor: colors.blue[600],
      borderColor: '#8999db',
      borderWidth: 2,
    },
    '&:hover $notchedOutline': {
      borderColor: colors.blue[300],
    },
  },
  notchedOutline: {
    borderColor: colors.grey[600],
    '&$focused': {
      color: 'blue',
    },
  },

};

export default MuiOutlinedInput;
